/**
 * SpaceX React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import { Root, Tabs } from './src/config/RouterNavigation'
import {View} from 'react-native'

export default class App extends Component {
  render() {
    console.disableYellowBox = true;
    return <Root />;
  }
}