import '../Globals';
import React, { Component } from 'react';
import { ScrollView, View, Text, Image } from 'react-native'
import { Card, ListItem, Button } from 'react-native-elements'

class About extends Component {
  constructor() {
    super()
    this.state = {
      data: [],
      loading: true
    }
  }

  getData() {
    fetch(API + 'info', { method: 'GET' })
    .then((response) => response.json())
    .then((responseJson) => { 
      this.setState({
        data: responseJson,
        loading: false
      })
    })
    .catch((error) => {
      console.error(error);
    })
  }

  componentDidMount() {
    this.getData();
  }

  render() {
    var headquartersObject = this.state.data.headquarters;
    if (this.state.loading) {
      return (
      <View>
        <Text style={{ marginTop: 20, textAlign: 'center'}}>Loading...</Text>
      </View>
      )
    }
    for (var key in headquartersObject) {
      var address = headquartersObject['address'] + ", " + headquartersObject['state'] + ", " + headquartersObject['city']
    }
    return (
      <ScrollView>

        <Card title={ this.state.data.name }>
          <View>
            <Text>SpaceX was founded in {this.state.data.founded}</Text>
            <Text>Employees: {this.state.data.employees}</Text>
            <Text>Vehicles: { this.state.data.vehicles }</Text>
            <Text>Launch Sites: { this.state.data.launch_sites }</Text>
            <Text>Test Sites: { this.state.data.test_sites }</Text>
          </View>
        </Card>

        <Card title="Team">
          <View>
            <Text>CEO: { this.state.data.ceo }</Text>
            <Text>CTO: { this.state.data.cto }</Text>
            <Text>CTO Propulsion: { this.state.data.cto_propulsion }</Text>
          </View>
        </Card>

        <Card title="Summary">
          <View>
            <Text>Valuation: { this.state.data.valuation }</Text>
            <Text>Address: { address }.</Text>
            <Text>Summary: { this.state.data.summary }</Text>
          </View>
        </Card>

      </ScrollView>
    );
  }
}

export default (About);
