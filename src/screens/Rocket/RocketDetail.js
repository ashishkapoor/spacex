import '../../Globals';
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
} from 'react-native';
import {
  Card,
  ListItem,
} from 'react-native-elements'

let rocketData = null;

class RocketDetail extends Component {
  state = {
    loading: true,
  }
  
  componentDidMount(){
    rocketData = this.props.navigation.state.params;
    this.setState ({
      loading: false,
    })
  }
  
  render() {
    if (this.state.loading) {
      return (
        <View>
          <Text style={ styles.loadingText }>Loading...</Text>
        </View>
      )
    }
    console.log('rocketData: ', rocketData);
    return (
      <ScrollView>
        <Card title={ rocketData.name }>
          <View>
            <Text style={ styles.rocketText }>Rocket Name: {rocketData.name}</Text>
            <Text style={ styles.rocketText }>Cost per launch: {rocketData.cost_per_launch}</Text>
            <Text style={ styles.rocketText }>Country: { rocketData.country }</Text>
            <Text style={ styles.rocketText }>Description: { rocketData.description }</Text>
            <Text style={ styles.rocketText }>First Flight: { rocketData.first_flight }</Text>
            <Text style={ styles.rocketText }>Diameter: { rocketData.diameter.feet } Feet/ { rocketData.diameter.meters } meters</Text>
            <Text style={ styles.rocketText }>Boosters: { rocketData.boosters }</Text>
            <Text style={ styles.rocketText }>Active: { rocketData.active && rocketData.active ? 'Yes' : 'No' }</Text>
            <Text style={ styles.rocketText }>Stages: {rocketData.stages}</Text>
            <Text style={ styles.rocketText }>Success rate pct: {rocketData.success_rate_pct}</Text>
            <Text style={ styles.rocketText }>Mass: {rocketData.mass.kg} kg/ {rocketData.mass.lb}lb</Text>
          </View>
        </Card>

        <Card title="Engines">
          <View>
            <Text style={ styles.rocketText }>Engine loss max: {rocketData.engines.engine_loss_max}</Text>
            <Text style={ styles.rocketText }>Layout: {rocketData.engines.layout}</Text>
            <Text style={ styles.rocketText }>Number: {rocketData.engines.number}</Text>
            <Text style={ styles.rocketText }>Propellant 1: {rocketData.engines.propellant_1}</Text>
            <Text style={ styles.rocketText }>propellant 2: {rocketData.engines.propellant_2}</Text>
            <Text style={ styles.rocketText }>Thrust sea level: {rocketData.engines.thrust_sea_level.kN} kN/{rocketData.engines.thrust_sea_level.lbf} lbf</Text>
            <Text style={ styles.rocketText }>Thrust to weight: {rocketData.engines.thrust_to_weight}</Text>
            <Text style={ styles.rocketText }>Thrust vacuum: {rocketData.engines.thrust_vacuum.kN} kN/ {rocketData.engines.thrust_vacuum.lbf} lbf</Text>
            <Text style={ styles.rocketText }>Type: {rocketData.engines.type}</Text>
            <Text style={ styles.rocketText }>Version: {rocketData.engines.version}</Text>
          </View>
        </Card>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  rocketText: {
    marginBottom: 10,
    fontSize: 14,
  },
  loadingText: {
    marginTop: 20,
    textAlign: 'center',
  },
});

export default RocketDetail;
