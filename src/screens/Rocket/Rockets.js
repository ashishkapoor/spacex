import '../../Globals';
import React, { Component } from 'react';
import { ScrollView, View, Text, FlatList, StyleSheet } from 'react-native';
import { List, ListItem } from 'react-native-elements'

class Rockets extends Component {
  state = {
    loading: true,
    data: [],
  }
  
  getData() {
    fetch(API + 'rockets/', { method: 'GET' })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          data: responseJson,
          loading: false,
        })
      })
      .catch((error) => {
        console.error(error);
      });
  }
  
  componentDidMount() {
    this.getData();
  }

  searchText = (text) => {
    console.log(text);
  }
  
  render() {
    if (this.state.loading) {
      return (
        <View>
          <Text style={{ marginTop: 20, textAlign: 'center'}}>Loading...</Text>
        </View>
      )
    }
    let latestData = this.state.data;
    return (
      <ScrollView style={{ marginTop: -20 }} >
        <List>
          {
            latestData.map((l, i) => (
              <ListItem
                key={i}
                title={l.name}
                subtitle={ l.type }
                onPress={() => this.props.navigation.navigate('RocketDetail', l)}
              />
            ))
          }
        </List>
      </ScrollView>
    )
  }
}

export default Rockets;
