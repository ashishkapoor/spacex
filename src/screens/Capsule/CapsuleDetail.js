import '../../Globals'
import React, { Component } from 'react'
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
} from 'react-native'
import {
  Card,
  ListItem,
} from 'react-native-elements'

let capsuleData = null;

class CapsuleDetail extends Component {
  state = {
    loading: true,
  }
  
  componentDidMount(){
    capsuleData = this.props.navigation.state.params;
    this.setState ({
      loading: false,
    })
  }
  
  render() {
    if (this.state.loading) {
      return (
        <View>
          <Text style={ styles.loadingText }>Loading...</Text>
        </View>
      )
    }

    return (
      <ScrollView>
        <Card title= {capsuleData.name } >
          <View>
            <Text style={ styles.rocketText }>Active: { capsuleData.active && capsuleData.active ? 'Yes' : 'No' }</Text>
            <Text style={ styles.rocketText }>Crew capacity: {capsuleData.crew_capacity}</Text>
            <Text style={ styles.rocketText }>Diameter: { capsuleData.diameter.feet } feet/{ capsuleData.diameter.meters } meters </Text>
            <Text style={ styles.rocketText }>Launch payload mass: { capsuleData.launch_payload_mass.kg } kg/ { capsuleData.launch_payload_mass.lb } lb/ </Text>
            <Text style={ styles.rocketText }>Launch payload vol: { capsuleData.launch_payload_vol.cubic_feet }cubic feet/{ capsuleData.launch_payload_vol.cubic_meters }cubic_meters </Text>
            <Text style={ styles.rocketText }>Orbit Duration yr: { capsuleData.orbit_duration_yr }</Text>
            <Text style={ styles.rocketText }>Sidewall angle deg: { capsuleData.sidewall_angle_deg }</Text>
            <Text style={ styles.rocketText }>Type: { capsuleData.type }</Text>
          </View>
        </Card>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  rocketText: {
    marginBottom: 10,
    fontSize: 14,
  },
  stretch: {
    width: 300,
    height: 200
  },
  loadingText: {
    marginTop: 20,
    textAlign: 'center',
  },
});

export default CapsuleDetail;
