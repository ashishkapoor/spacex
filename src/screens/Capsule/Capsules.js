import '../../Globals';
import React, { Component } from 'react';
import { ScrollView, View, Text, FlatList, StyleSheet } from 'react-native';
import { List, ListItem } from 'react-native-elements'

class Capsules extends Component {
  state = {
    loading: true,
    data: [],
  }
  
  getData() {
    fetch(API + 'capsules/', { method: 'GET' })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          data: responseJson,
          loading: false
        })
      })
      .catch((error) => {
        console.error(error);
      });
  }
  
  componentDidMount() {
    this.getData();
  }
  
  timeConverter(UNIX_timestamp) {
    var date = new Date(UNIX_timestamp * 1000);
    var formattedDate = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' ' + ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2);
    return formattedDate;
  }
  
  render() {
    if (this.state.loading) {
      return (
        <View>
          <Text style={{ marginTop: 20, textAlign: 'center'}}>Loading...</Text>
        </View>
      )
    }
    let latestData = this.state.data.reverse();
    console.log(this.state.data);
    return (
      <ScrollView style={{ marginTop: -20 }} >
        <List>
          {
            latestData.map((l, i) => (
              <ListItem
                key={i}
                title={l.name}
                subtitle={ l.active && l.active ? "Active" : "Inactive" }
                onPress={() => this.props.navigation.navigate('CapsuleDetail', l)}
              />
            ))
          }
        </List>
      </ScrollView>
    )
  }
}

export default (Capsules);
