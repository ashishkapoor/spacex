import '../../Globals';
import React, { Component } from 'react';
import { ScrollView, View, Text, FlatList, StyleSheet } from 'react-native';
import { List, ListItem, SearchBar } from 'react-native-elements'
import { isEmpty } from 'lodash';

class Launches extends Component {
  state = {
    loading: true,
    data: {},
    filtered: {},
  }
  
  getData() {
    fetch(API + 'launches/', { method: 'GET' })
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        data: responseJson,
        loading: false,
      })
    })
    .catch((error) => {
      console.error(error);
    });
  }
  
  componentDidMount() {
    this.getData();
  }

  searchText = (text) => {
    this.setState({
      filtered: this.state.data.filter(data => {
        return data.rocket.rocket_name.indexOf(text) !== -1;
      }),
    });
  }

  timeConverter(UNIX_timestamp) {
    var date = new Date(UNIX_timestamp * 1000);
    var formattedDate = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();
    return formattedDate;
  }

  render() {
    if (this.state.loading) {
      return (
      <View>
        <Text style={{ marginTop: 20, textAlign: 'center' }}>Loading...</Text>
      </View>
      )
    }
    let dataSource = null;
    const { filtered, data } = this.state;
    if (isEmpty(filtered)) {
      dataSource = data;
    } else {
      dataSource = filtered;
    }
    return (
      <ScrollView style={{ marginTop: -20 }} >
        <List>
          <SearchBar
              lightTheme
              onChangeText={this.searchText}
              icon={{ type: 'font-awesome', name: 'search' }}
              placeholder='Type Here...' />
          {
            dataSource.reverse().map((l, i) => (
              <ListItem
                roundAvatar
                avatar={{uri:l.links.mission_patch}}
                key={i}
                title={l.rocket.rocket_name}
                subtitle={this.timeConverter(l.launch_date_unix)}
                onPress={() => this.props.navigation.navigate('LaunchDetail', l)}
              />
            ))
          }
        </List>
      </ScrollView>
    )
  }
}

export default (Launches);
