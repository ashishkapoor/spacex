import '../../Globals'
import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  Image,
} from 'react-native';
import {
  Card,
  ListItem,
  Button
} from 'react-native-elements'

let launchData = null;

class LaunchpadDetail extends Component {
  state = {
    loading: true,
  }
  
  componentDidMount(){
    launchData = this.props.navigation.state.params;
    this.setState ({
      loading: false,
    })
  }

  render() {
    if (this.state.loading) {
      return (
        <View>
          <Text style={ styles.loadingText }>Loading...</Text>
        </View>
      )
    }
    return (
      <ScrollView>
        <Card title="Flights">
          <View>
            {/* <Image
              source={{ uri: launchData.links.mission_patch }}
              style={{ height: 360, width: 300 }}
              resizeMode="cover"
            /> */}
            <Text style={ styles.rocketText }>Flight number: {launchData.flight_number}</Text>
            <Text style={ styles.rocketText }>Launch date: { launchData.launch_date_unix }</Text>
            <Text style={ styles.rocketText }>Launch site id: { launchData.launch_site.site_id } </Text>
            <Text style={ styles.rocketText }>Launch site name:{launchData.launch_site.site_name} </Text>
            <Text style={ styles.rocketText }>Launch site long name: {launchData.launch_site.site_name_long} </Text>
            <Text style={ styles.rocketText }>Launch success: { launchData.launch_success && launchData.launch_success ? 'Yes' : 'No' }</Text>
            <Text style={ styles.rocketText }>Launch year: { launchData.launch_year }</Text>
            <Text style={ styles.rocketText }>Telemetry; Flight club: { launchData.telemetry.flight_club }</Text>
            <Text style={ styles.rocketText }>Details: {launchData.details}</Text>
          </View>
        </Card>
        <Card title="Links">
          <View>
            <Text style= {{ fontWeight: 'bold' }} >Article link:</Text><Text style={ styles.rocketText } selectable>{ launchData.links.article_link }</Text>
            <Text style= {{ fontWeight: 'bold' }} >Mission patch:</Text><Text style={ styles.rocketText } selectable>{launchData.links.mission_patch}</Text>
            <Text style= {{ fontWeight: 'bold' }} >Presskit:</Text><Text style={ styles.rocketText } selectable>{ launchData.links.presskit }</Text>
            <Text style= {{ fontWeight: 'bold' }} >Reddit campaign:</Text><Text style={ styles.rocketText } selectable>{ launchData.links.reddit_campaign }</Text>
            <Text style= {{ fontWeight: 'bold' }} >Reddit launch:</Text><Text style={ styles.rocketText } selectable>{ launchData.links.reddit_launch }</Text>
            <Text style= {{ fontWeight: 'bold' }} >Reddit media:</Text><Text style={ styles.rocketText } selectable>{ launchData.links.reddit_media }</Text>
            <Text style= {{ fontWeight: 'bold' }} >Reddit recovery:</Text><Text style={ styles.rocketText } selectable>{ launchData.links.reddit_recovery }</Text>
            <Text style= {{ fontWeight: 'bold' }} >Video link:</Text><Text style={ styles.rocketText } selectable>{ launchData.links.video_link && launchData.links.video_link }</Text>
          </View>
        </Card>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  rocketText: {
    marginBottom: 10,
    fontSize: 14,
  },
  stretch: {
    width: 300,
    height: 200
  },
  loadingText: {
    marginTop: 20,
    textAlign: 'center',
  },
});

export default LaunchpadDetail;
