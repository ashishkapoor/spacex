import '../../Globals'
import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
} from 'react-native'
import {
  Card,
  ListItem,
} from 'react-native-elements'

let launchpadData = null;

class LaunchpadDetail extends Component {
  state = {
    loading: true,
  }
  
  componentDidMount(){
    launchpadData = this.props.navigation.state.params;
    this.setState ({
      loading: false,
    })
  }
  
  render() {
    if (this.state.loading) {
      return (
        <View>
          <Text style={{ marginTop: 20, textAlign: 'center'}}>Loading...</Text>
        </View>
      )
    }
    console.log('launchpadData: ', launchpadData);
    return (
      <ScrollView>
        <Card title={ launchpadData.full_name }>
          <View>
            <Text style={ styles.rocketText }>Launchpad ID: {launchpadData.id}</Text>
            <Text style={ styles.rocketText }>Status: {launchpadData.status}</Text>
            <Text style={ styles.rocketText }>Location: {launchpadData.location.name}, {launchpadData.location.region } </Text>
            <Text style={ styles.rocketText }>Vehicles launched: {launchpadData.vehicles_launched.length }</Text>
            <Text style={ styles.rocketText }>Details: {launchpadData.details}</Text>
          </View>
        </Card>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  rocketText: {
    marginBottom: 10,
    fontSize: 14,
  },
  stretch: {
    width: 300,
    height: 200
  },
  loadingText: {
    marginTop: 20,
    textAlign: 'center',
  },
});

export default LaunchpadDetail;
