import React from 'react';
import { StackNavigator } from 'react-navigation';
import Launch from '../screens/Launch/Launches';
import Rocket from '../screens/Rocket/Rockets';
import Capsule from '../screens/Capsule/Capsules';
import Launchpad from '../screens/Launchpad/Launchpads'
import About from '../screens/About'
import LaunchDetail from '../screens/Launch/LaunchDetail'
import RocketDetail from '../screens/Rocket/RocketDetail'
import LaunchpadDetail from '../screens/Launchpad/LaunchpadDetail'
import CapsuleDetail from '../screens/Capsule/CapsuleDetail'

export const HomeStack = StackNavigator({
  Launch: {
    screen: Launch,
    navigationOptions: {
      title: "Launches",
      headerTintColor: '#000',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  },
  LaunchDetail: {
    screen: LaunchDetail,
    navigationOptions: {
      title: "Launch",
      headerTintColor: '#000',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  }
});

export const RocketStack = StackNavigator({
  Rocket: {
    screen: Rocket,
    navigationOptions: {
      title: "Rockets",
      headerTintColor: '#000',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  },
  RocketDetail: {
    screen: RocketDetail,
    navigationOptions: {
      title: "Launch",
      headerTintColor: '#000',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  }
});

export const LaunchpadStack = StackNavigator({
  Launchpad: {
    screen: Launchpad,
    navigationOptions: {
      title: "Launchpads",
      headerTintColor: '#000',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  },
  LaunchpadDetail: {
    screen: LaunchpadDetail,
    navigationOptions: {
      title: "Launch",
      headerTintColor: '#000',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  }
});

export const CapsuleStack = StackNavigator({
  Capsule: {
    screen: Capsule,
    navigationOptions: {
      title: "Capsules",
      headerTintColor: '#000',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  },
  CapsuleDetail: {
    screen: CapsuleDetail,
    navigationOptions: {
      title: "Capsule",
      headerTintColor: '#000',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  }
});

export const AboutStack = StackNavigator({
  About: {
    screen: About,
    navigationOptions: {
      title: "About",
      headerTintColor: '#000',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  }
});

