import { StackNavigator } from 'react-navigation'
import { Tabs } from './TabNavigation'

export const Root = StackNavigator (
  {
    Tabs: {
      screen: Tabs
    }
  },
  {
    mode: "modal",
    headerMode: 'none',
	}
);