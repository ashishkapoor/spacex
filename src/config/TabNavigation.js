import React from 'react';
import {
  Text,
  Image,
  StyleSheet
} from 'react-native';
import {
  TabNavigator
} from 'react-navigation';
import {
  HomeStack,
  RocketStack,
  CapsuleStack,
  LaunchpadStack,
  AboutStack
} from './StackNavigation';

export const Tabs = TabNavigator({
  Home: {
    screen: HomeStack,	
    navigationOptions: {
			tabBarLabel: ({ tintColor, focused }) => (
        <Text style={[styles.tabLabel, {color: focused ? '#000' : 'grey'}]}>Launches</Text>
      ),
      tabBarIcon: ({ tintColor, focused }) => (
				<Image 
					style={[styles.icon, {tintColor: "#000"}]}
          source={focused ? require('../../tab-bar-img/rocket_filled.png') : require('../../tab-bar-img/rocket.png')}
				/>
      )
    }
  },
  Rocket: {
    screen: RocketStack,
    navigationOptions: {
      tabBarLabel: ({ tintColor, focused }) => (
        <Text style={[styles.tabLabel, {color: focused ? '#000' : 'grey'}]}>Rockets</Text>
      ),
      tabBarIcon: ({ tintColor, focused }) => (
        <Image 
          style={[styles.icon, {tintColor: "#000"}]}
          source={focused ? require('../../tab-bar-img/missile_filled.png') : require('../../tab-bar-img/missile.png')}
        />
      )
    }
  },
  Capsule: {
    screen: CapsuleStack,
    navigationOptions: {
      tabBarLabel: ({ tintColor, focused }) => (
        <Text style={[styles.tabLabel, {color: focused ? '#000' : 'grey'}]}>Capsules</Text>
      ),
      tabBarIcon: ({ tintColor, focused }) => (
        <Image
          style={[styles.icon, {tintColor: "#000"}]}
          source={focused ? require('../../tab-bar-img/pill_filled.png') : require('../../tab-bar-img/pill.png')}
        />
      )
    }
  },
  Launchpad: {
    screen: LaunchpadStack,
    navigationOptions: {
      tabBarLabel: ({ tintColor, focused }) => (
        <Text style={[styles.tabLabel, {color: focused ? '#000' : 'grey'}]}>Launchpads</Text>
      ),
      tabBarIcon: ({ tintColor, focused }) => (
        <Image
          style={[styles.icon, {tintColor: "#000"}]}
          source={focused ? require('../../tab-bar-img/plus_2_math_filled.png') : require('../../tab-bar-img/plus_2_math.png')}
        />
      )
    }
  },
  About: {
    screen: AboutStack,
    navigationOptions: {
      tabBarLabel: ({ tintColor, focused }) => (
        <Text style={[styles.tabLabel, {color: focused ? '#000' : 'grey'}]}>About</Text>
      ),
      tabBarIcon: ({ tintColor, focused }) => (
        <Image 
          style={[styles.icon, {tintColor: "#000"}]}
          source={focused ? require('../../tab-bar-img/info_filled.png') : require('../../tab-bar-img/info.png')}
        />
      )
    }
	},
});

const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  },
  tabLabel: {
    textAlign: 'center',
    fontSize: 10
  }
});